== Using Asciidoctor with Maven

.pom.xml
[source,xml]
----
<plugins>
    <plugin>
        <groupId>org.asciidoctor</groupId>
        <artifactId>asciidoctor-maven-plugin</artifactId>
        <version>1.5.8</version>
        ...
    </plugin>
</plugins>
----

Sources go in `${basedir}/src/main/asciidoc`.
Output goes to `${project.build.directory}/generated-docs`.

=== Using Asciidoc for Maven Site

* Place documents in `src/site/asciidoc`.
* Runs independently from `src/main/asciidoc`.