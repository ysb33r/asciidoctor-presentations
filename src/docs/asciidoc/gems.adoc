== Adding GEMs


[source,groovy,subs=+attributes]
----
plugins {
  id 'org.asciidoctor.jvm.gems' version '{pluginVersion}'
}

repositories {
  maven { url 'http://rubygems-proxy.torquebox.org/releases' }
}

dependencies {
  asciidoctorGems 'rubygems:asciidoctor-bibtex:0.3.1'
}

asciidoctorj {
  requires 'asciidoctor-bibtex'
}

asciidoctor {
  dependsOn asciidoctorGems
----

