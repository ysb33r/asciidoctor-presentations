== Writing Javadoc in Asciidoc

[source,java]
----
/**
 * <h1>Asciidoclet</h1>
 *
 * <p>Sample comments that include {@code source code}.</p>
 *
 * <pre>{@code
 * public class Asciidoclet extends Doclet {
 *     private final Asciidoctor asciidoctor = Asciidoctor.Factory.create();
 *
 *     {@literal @}SuppressWarnings("UnusedDeclaration")
 *     public static boolean start(RootDoc rootDoc) {
 *         new Asciidoclet().render(rootDoc);
 *         return Standard.start(rootDoc);
 *     }
 * }
 * }</pre>
 *
 * @author <a href="https://github.com/johncarl81">John Ericksen</a>
 */
public class Asciidoclet extends Doclet {
}
----

=== Writing Javadoc with Asciidoclet

[source,java]
----
/**
 * = Asciidoclet
 *
 * Sample comments that include `source code`.
 *
 * [source,java]
 * --
 * public class Asciidoclet extends Doclet {
 *     private final Asciidoctor asciidoctor = Asciidoctor.Factory.create();
 *
 *     @SuppressWarnings("UnusedDeclaration")
 *     public static boolean start(RootDoc rootDoc) {
 *         new Asciidoclet().render(rootDoc);
 *         return Standard.start(rootDoc);
 *     }
 * }
 * --
 *
 * @author https://github.com/johncarl81[John Ericksen]
 */
public class Asciidoclet extends Doclet {
}
----

=== Example API docs - Stormpot

image::images/asciidoclet-output-example.png[]

Source: http://chrisvest.github.io/stormpot/site/apidocs/stormpot/Poolable.html

=== Using Asciidoclet with Build Tooling

* Maven: `maven-javadoc-plugin` plugin.
* Gradle: `asciidoclet` artifact.
* Ant: Via `javadoc` task.

=== JDK support

* Up to JDK8.
* Final changes being made for support JDK11+