== !

Gradle is more than a tool for building Java/Groovy/Kotlin etc.

It is an advanced pipeline automation tool.

== !

Asciidoctor plugin for Gradle is (probably) the most advanced integration between build tooling and documentation production.